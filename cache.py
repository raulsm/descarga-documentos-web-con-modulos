#!/usr/bin/python3

#Importamos la clase Robot
from robot import Robot

class Cache:
    def __init__(self):
        # Inicializamos Cache con un diccionario vacío para almacenar las URLs descargadas y su contenido
        self.cache = {}

    def retrieve(self, url):
        #Recuperamos el contenido de la URL utilizando un objeto Robot si aún no
        # ha sido descargado
        if url not in self.cache:
            #Creamos un objeto Robot para descargar el contenido
            robot = Robot(url=url)
            #Obtenemos el contenido con el objeto Robot y lo almacenamos en el diccionario
            self.cache[url] = robot.content()

    def show(self, url):
        #Mostramos el contenido
        if url not in self.cache:
            #Descargamos la URL si no está en el diccionario
            self.retrieve(url)
        #Imprimimos la URL y su contenido
        print(url)
        print(self.cache[url])

    def show_all(self):
        #Mostramos todas las URLs descargadas
        print("Se han descargado las siguientes URLs:")
        for numero, url in enumerate(self.cache):
            #Imprimimos el nº de la URL y su URL
            print(f"{numero}. {url}")

    def content(self, url):
        #Devolvemos el contenido de la URL especificada
        if url not in self.cache:
            #Descargamos la URL si no está en el diccionario
            self.retrieve(url)
        #Devolvemos el contenido de la URL
        return self.cache[url]

if __name__ == "__main__":
    #Ejemplo 1
    cache_1 = Cache()
    cache_1.retrieve("https://www.urjc.es/")
    cache_1.show("https://www.urjc.es/")
    cache_1.show_all()
    cache_1.content("https://www.python.org/")
    cache_1.show_all()

    #Ejemplo 2
    cache_2 = Cache()
    cache_2.show("https://github.com/CursosWeb")
    cache_2.retrieve("https://github.com/CursosWeb")
    cache_2.show_all()
    cache_2.content("https://github.com/CursosWeb/Code/tree/master/Python-Django")
    cache_2.show_all()
