#!/usr/bin/python3

import urllib.request

class Robot:
    def __init__(self, url):
        #Inicializamos Robot con la URL dada
        # y establecemos __data_text en None
        self.url = url
        self.__data_text = None

    def retrieve(self):
        #Descargamos el contenido de la URL si aún no se descargo
        if self.__data_text is None:
            try:
                #Abrimos la URL y leemos su contenido
                endpoint = urllib.request.urlopen(self.url)
                self.__data_text = endpoint.read().decode("utf-8")
                print(f"Descargando la URL ({self.url})")
            except urllib.error.HTTPError as e:
                #Manejo de errores HTTP
                print(f"Error durante la descarga de {self.url}")
            except urllib.error.URLError as e:
                #Manejo de errores de URL
                print(f"Error: la URL ({self.url}) no es correcta")
        else:
            # Si __data_text no es None, significa que la URL ya ha sido descargada
            print(f"La URL {self.url} ya ha sido descargada")

    def show(self):
        # Mostramos el contenido de la URL
        self.retrieve()
        print(self.__data_text)

    def content(self):
        # Devolvemos el contenido de la URL
        self.retrieve()
        return self.__data_text


if __name__ == "__main__":

    #Ejemplo 1
    robot_1 = Robot("https://www.urjc.es/")
    robot_1.retrieve()
    robot_1.show()
    robot_1.content()

    #Ejemplo 2
    robot_2 = Robot("https://www.aulavirtual.urjc.es/")
    robot_2.retrieve()
    robot_2.show()
    robot_2.content()



