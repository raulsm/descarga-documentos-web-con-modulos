#!/usr/bin/python3

from cache import Cache
from robot import Robot

if __name__ == "__main__":

    # Ejemplo Cache 1
    cache_1 = Cache()
    cache_1.retrieve("https://www.urjc.es/")
    cache_1.show("https://www.urjc.es/")
    cache_1.show_all()
    cache_1.content("https://www.python.org/")
    cache_1.show_all()

    # Ejemplo Cache 2
    cache_2 = Cache()
    cache_2.show("https://github.com/CursosWeb")
    cache_2.retrieve("https://github.com/CursosWeb")
    cache_2.show_all()
    cache_2.content("https://github.com/CursosWeb/Code/tree/master/Python-Django")
    cache_2.show_all()

    # Ejemplo Robot 1
    robot_1 = Robot("https://www.urjc.es/")
    robot_1.retrieve()
    robot_1.show()
    robot_1.content()

    # Ejemplo Robot 2
    robot_2 = Robot("https://www.aulavirtual.urjc.es/")
    robot_2.retrieve()
    robot_2.show()
    robot_2.content()



